import tensorflow
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D, MaxPooling2D
from tensorflow.keras.layers import Activation, Dropout, Flatten, Dense
from tensorflow.keras import backend as KBackend
from tensorflow.keras.callbacks import ModelCheckpoint
from tensorflow.keras.utils import plot_model
import matplotlib.pyplot as plt
import keras,os
from keras.models import Sequential
from keras.layers import Dense, Conv2D, MaxPool2D , Flatten
from keras.preprocessing.image import ImageDataGenerator
import numpy as np
training_dataset='C:/Users/lizey/summerstudio2020/race_classification/train/'
testing_dataset='C:/Users/lizey/summerstudio2020/race_classification/test'
validation_dataset='C:/Users/lizey/summerstudio2020/race_classification/validation'
# DATASET_PATH = "C:/Users/User/lizey/SummerStudio2020/race_classification"
# print(os.listdir(DATASET_PATH))

# df = pd.read_csv(DATASET_PATH + "train.csv", nrows=5000, error_bad_lines=False)

img_width, img_height = 250,250
train_data_dir = 'C:/Users/lizey/summerstudio2020/race_classification/train'
validation_data_dir = 'C:/Users/lizey/summerstudio2020/race_clascsification/test'
nb_train_samples = 240
nb_validation_samples = 130

# Define epoch for training
epochs =5
# Lower the batch size if you're training on GPU and get out of memory errors
batch_size =30
if KBackend.image_data_format() == 'channels_first':

    input_shape = (1, img_width, img_height)
else:
    input_shape = (img_width, img_height,1)


train_datagen = ImageDataGenerator(
    rescale=1. / 255,
    shear_range=0.2,
    zoom_range=0.2,
    horizontal_flip=True)
#
datagen = ImageDataGenerator()
# This is the augmentation configuration we will use for validation:
# only perform a image rescaling
validation_datagen = ImageDataGenerator(rescale=1. / 255)


# Generates the data files for training with a binary  mode
traindata = train_datagen.flow_from_directory(training_dataset,
    target_size=(img_width, img_height),
    batch_size=batch_size, class_mode='binary')


# Generates the data files for validation with a binary class mode
testdata = validation_datagen.flow_from_directory(validation_dataset,
    target_size=(img_width, img_height),
    batch_size=batch_size, class_mode='binary')
# A simple 3 layer convolution network with ReLU activation

model = Sequential()
model.add(Conv2D(input_shape=(250,250,3),filters=64,kernel_size=(3,3),padding="same", activation="relu"))
model.add(Conv2D(filters=64,kernel_size=(3,3),padding="same", activation="relu"))
model.add(MaxPool2D(pool_size=(2,2),strides=(2,2)))
model.add(Conv2D(filters=128, kernel_size=(3,3), padding="same", activation="relu"))
model.add(Conv2D(filters=128, kernel_size=(3,3), padding="same", activation="relu"))
model.add(MaxPool2D(pool_size=(2,2),strides=(2,2)))
model.add(Conv2D(filters=256, kernel_size=(3,3), padding="same", activation="relu"))
model.add(Conv2D(filters=256, kernel_size=(3,3), padding="same", activation="relu"))
model.add(Conv2D(filters=256, kernel_size=(3,3), padding="same", activation="relu"))
model.add(MaxPool2D(pool_size=(2,2),strides=(2,2)))
model.add(Conv2D(filters=512, kernel_size=(3,3), padding="same", activation="relu"))
model.add(Conv2D(filters=512, kernel_size=(3,3), padding="same", activation="relu"))
model.add(Conv2D(filters=512, kernel_size=(3,3), padding="same", activation="relu"))
model.add(MaxPool2D(pool_size=(2,2),strides=(2,2)))
model.add(Conv2D(filters=512, kernel_size=(3,3), padding="same", activation="relu"))
model.add(Conv2D(filters=512, kernel_size=(3,3), padding="same", activation="relu"))
model.add(Conv2D(filters=512, kernel_size=(3,3), padding="same", activation="relu"))
model.add(MaxPool2D(pool_size=(2,2),strides=(2,2)))
model.add(Flatten())
model.add(Dense(units=4096,activation="relu"))
model.add(Dense(units=4096,activation="relu"))
model.add(Dense(units=2, activation="softmax"))
from keras.optimizers import Adam
opt = Adam(lr=0.001)
model.compile(optimizer=opt, loss=keras.losses.categorical_crossentropy, metrics=['accuracy'])
from keras.callbacks import ModelCheckpoint, EarlyStopping
checkpoint = ModelCheckpoint("vgg16_1.h5", monitor='val_acc', verbose=1, save_best_only=True, save_weights_only=False, mode='auto', period=1)
early = EarlyStopping(monitor='val_acc', min_delta=0, patience=20, verbose=1, mode='auto')
hist = model.fit_generator(steps_per_epoch=100,generator=traindata, validation_data= testdata, validation_steps=10,epochs=100,callbacks=[checkpoint,early])
# model=Sequential()
# #Layer 1 - A 2D Convolution Layer with 32 filters, a kernal size of 3,3 and a 1,1 stride
# #          and an input shape defined by the shape of our image.
# #          It has a ReLu activation function and we include a 2D Max Pooling layer with a pool size of 2,2
#
#
# model.add(Conv2D(32, kernel_size=(3, 3),strides=(1, 1), input_shape=input_shape))
# model.add(Activation('relu'))
# model.add(MaxPooling2D(pool_size=(2, 2)))
#
# #Layer 2 - A 2D Convolution Layer with 32 filters, a kernal size of 3,3 and a 1,1 stride
# #          It has a ReLu activation function and we include a 2D Max Pooling layer with a pool size of 2,2
# model.add(Conv2D(32, kernel_size=(3, 3),strides=(1, 1)))
# model.add(Activation('relu'))
# model.add(MaxPooling2D(pool_size=(2, 2)))
#
# #Layer 3 - A 2D Convolution Layer with 64 filters and a kernal size of 3,3.
# #          It has a ReLu activation function and we include a 2D Max Pooling layer with a pool size of 2,2
# model.add(Conv2D(64, kernel_size=(3, 3)))
# model.add(Activation('relu'))
# print("Prior to flattening", model.output_shape)




# #Layer 4 - A flatten layer.
# #         This reshapes the array from a 4D to a 1D array
# #         (We previously did this BEFORE during our data preperation, now we need to do it in our network itself)
# # model.add(Flatten())
#
# #
# # print("After flattening", model.output_shape)
#
# #Layer 5 - A Dense Layer with 64 Neurons, a ReLu activation function and a dropout of 0.5
#
# # model.add(Dense(64))
# # model.add(Activation('relu'))
# # model.add(Dropout(0.5))
#
# #
# #
# # #Layer 6 - An output layer with one output and a sigmoid activation layer
# # model.add(Dense(1))
# # model.add(Activation('sigmoid'))
#
# # Picking a binary crossentropy loss function since we only have 2 classes
# model.compile(loss='binary_crossentropy',
#               optimizer='rmsprop',
#               metrics=['accuracy'])
#
# # Print a summary of the network
# print(model.summary())
#
# model.compile(loss='binary_crossentropy',
#               optimizer='rmsprop',
#               metrics=['accuracy'])
#
# plot_model(model, to_file='model.png')
#
# # This is the data augmentation configuration we will use for training
# train_datagen = ImageDataGenerator(
#     rescale=1. / 255,
#     shear_range=0.2,
#     zoom_range=0.2,
#     horizontal_flip=True)
# #
# datagen = ImageDataGenerator()
# This is the augmentation configuration we will use for validation:
# only perform a image rescaling
# # validation_datagen = ImageDataGenerator(rescale=1. / 255)
#
#
# # Generates the data files for training with a binary  mode
# train_generator = train_datagen.flow_from_directory(training_dataset,
#     target_size=(img_width, img_height),
#     batch_size=batch_size, class_mode='binary')
#
#
# # Generates the data files for validation with a binary class mode
# validation_generator = validation_datagen.flow_from_directory(validation_dataset,
#     target_size=(img_width, img_height),
#     batch_size=batch_size, class_mode='binary')
#
#
# # Define the checkpoint
# checkpoint = ModelCheckpoint('hotdoggo.h5', monitor='val_acc',
#                              verbose=1, save_best_only=True, save_weights_only=False, mode='max')
# callbacks_list = [checkpoint]
#
# # Train the model
# # The double // operator is just an easy python way of dividing then rounding up/down to the nearest whole number
# history = model.fit(
#     train_generator,
#     steps_per_epoch=nb_train_samples // batch_size,
#     epochs=epochs,
#     validation_data=validation_generator,
#     validation_steps=nb_validation_samples // batch_size,
#     callbacks = callbacks_list)
#
# # Plot training & validation accuracy values
# plt.plot(history.history['accuracy'])
# plt.plot(history.history['val_accuracy'])
# plt.title('Model accuracy')
# plt.ylabel('Accuracy')
# plt.xlabel('Epoch')
# plt.legend(['Train', 'Test'], loc='upper left')
# plt.show()
#
#
# # Plot training & validation loss values
# plt.plot(history.history['loss'])
# plt.plot(history.history['val_loss'])
# plt.title('Model loss')
# plt.ylabel('Loss')
# plt.xlabel('Epoch')
# plt.legend(['Train', 'Test'], loc='upper left')
# plt.show()
#
#
# # Save the weights for future use
# model.save_weights('race_with_vgg16.h5')
#
# # Now to run some predictions
# # If you want to reload the weights, define the model again as above then load the weights
# # model.load_weights('CNN_hot_dog.h5')
# # Load a single image from a folder and convert to a numpy array
#
# # from PIL import Image
# # from tensorflow.keras.preprocessing import image
# # import numpy as np
# # predict_data_dir = 'data/test'
# # classes = list(train_generator.class_indices.keys())
# #
# # predict_image = image.load_img(predict_data_dir + "/5.jpg")
# # predict_image_resized = predict_image.resize([img_width, img_height])
# #
# # predict_image_array = image.img_to_array(predict_image_resized)
# #
# # predict_image_array = np.expand_dims(predict_image_array, axis=0)
# #
# # prediction = model.predict(predict_image_array, verbose=1)
# # prediction_index = prediction.argmax()
# #
# # plt.imshow(predict_image)
# #
# # print("The Predicted Value is", prediction)
# #
# # print("The predicted item is a " + classes[int(prediction)])
#
#
#
#
# # 0000000000000000000
# # from PIL import Image
# # from tensorflow.keras.preprocessing import image
# # import numpy as np
# # predict_data_dir = 'data/test'
# # classes = list(train_generator.class_indices.keys())
# #
# # predict_image = image.load_img(predict_data_dir + "/5.jpg")
# # predict_image_resized = predict_image.resize([img_width, img_height])
# #
# # predict_image_array = image.img_to_array(predict_image_resized)
# #
# # predict_image_array = np.expand_dims(predict_image_array, axis=0)
# #
# # prediction = model.predict(predict_image_array, verbose=1)
# # prediction_index = prediction.argmax()
# #
# # plt.imshow(predict_image)
# #
# # print("The Predicted Value is", prediction)
# #
# # print("The predicted item is a " + classes[int(prediction)])
# #
# #
# # # Lets predict the rest of the images
# # import os, os.path
# # predict_data_dir = 'C:/Users/lizey/summerstudio2020/race classification/test'
# #
# # list_of_images = os.listdir(predict_data_dir)
# #
# # for i in range(len(list_of_images)):
# #     predict_image = image.load_img(predict_data_dir + list_of_images[i+1], target_size=(img_width, img_height))
# #     predict_image_resized = predict_image.resize([img_width, img_height])
# #     predict_image_array = image.img_to_array(predict_image_resized)
# #     predict_image_array = np.expand_dims(predict_image_array, axis=0)
# #
# #     prediction = model.predict(predict_image_array, verbose=1)
# #     prediction_index = prediction.argmax()
# #
# #     plt.figure()
# #     plt.imshow(predict_image)
# #     plt.show()
# #     print(prediction)
# #
# #     print("The predicted item is a " + classes[int(prediction)] + "\n\n")