import csv
import shutil


# Main Function
def main():
    excel = open('train.csv', newline='')

    # Initialize csvreader for dataset
    reader = csv.reader(excel)

    # Read data from reader
    data = list(reader)

    # Variables for progress counter
    lines = len(data)
    i = 0

    # Analyze data in dataset
    for row in data:
        image = row[0]  # Assign image name and state to variables
        state = row[1]

        # Print image information
        print('({}/{}) Processing image ({}): {}'.format(i + 1, lines, state, image))

        # Increment i
        i += 1
        if i > 1:
            try:
                shutil.move('C:/Users/lizey/summerstudio2020/week_challenge/train/' + image,
                            'C:/Users/lizey/summerstudio2020/week_challenge/train/' + state)
                print(
                    ' -> Moved to C:/Users/User/Documents/SummerStudio2020/Image Category/train/' + state)  # Inform the user of action being taken
            except FileNotFoundError:
                print(' -> Failed to find file')  # Inform the user of the failure
# Execute main function if name is equal to main
if __name__ == '__main__':
    main()
