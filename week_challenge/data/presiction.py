from keras.models import load_model
import cv2
import numpy as np
# import tensorflow as tf
# classifierLoad = tf.keras.models.load_model('hotdoggo.h5')
# model = load_model('hotdoggo.h5')

# classifierLoad.compile(loss='binary_crossentropy',
#               optimizer='rmsprop',
#               metrics=['accuracy'])
#
# img = cv2.imread('test/00a36d4d6d152404670276fc983273bc.jpg')
# img = cv2.resize(img,(200,240))
# img = np.reshape(img,[1,240,200,3])
#
# classes = classifierLoad.predict_classes(img)




# Modify 'test1.jpg' and 'test2.jpg' to the images you want to predict on

from keras.models import load_model
from keras.preprocessing import image
import numpy as np
import tensorflow as tf
model = tf.keras.models.load_model('hotdoggo.h5')

# dimensions of our images
img_width, img_height = 32, 32

# load the model we saved
# model = load_model('hotdoggo.h5')
model.compile(loss='binary_crossentropy',
              optimizer='rmsprop',
              metrics=['accuracy'])
Category=["cactus","not cactus"]
# predicting images
img = image.load_img('C:/Users/lizey/summerstudio2020/week_challenge/data/train/cactus/0a02cef73a1660adad8fc43b07fa9d23.jpg', target_size=(img_width, img_height))
x = image.img_to_array(img)
x = np.expand_dims(x, axis=0)

images = np.vstack([x])
classes = model.predict(images, batch_size=10)
print(Category[int(classes[0][0])])

# # predicting multiple images at once
# img = image.load_img('test2.jpg', target_size=(img_width, img_height))
# y = image.img_to_array(img)
# y = np.expand_dims(y, axis=0)
#
# # pass the list of multiple images np.vstack()
# images = np.vstack([x, y])
# classes = model.predict_classes(images, batch_size=10)
#
# # print the classes, the images belong to
# print classes
# print classes[0]
# print classes[0][0]
